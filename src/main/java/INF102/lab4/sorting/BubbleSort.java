package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
    	int n = list.size();
    	for (int i=0; i<n; i++) {
	    	for (int j=1; j<(n-i); j++) {
	    		T leftIndex = list.get(j-1);
	    		T rightIndex = list.get(j);
	    		if (leftIndex.compareTo(rightIndex) > 0) {
	    			swap(list, j);
	    		}
	    	}
    	}
    }
    
    private <T> void swap(List<T> list, int index) {
    	T temp;
    	T left = list.get(index-1);
    	T right = list.get(index);
    	
    	temp = list.set(index, left);
    	list.set(index-1, right);
    	list.set(index, temp);

	}
    
}
