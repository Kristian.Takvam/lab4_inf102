package INF102.lab4.median;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        
        // First we sort the array
        Collections.sort(list);
        int mid = listCopy.size() / 2;
        return listCopy.get(mid);

    }
    
    private <T> int partition(List<T> list, int left, int right) {
        int x = (int) list.get(right);
        int i = left;
        for (int j = left; j <= right - 1; j++) {
            if ((int) list.get(j) <= x) {
                swap(list, i, j);
                i++;
            }
        }
        swap(list, i, right);
        return i;
    }
    
    private <T> int select(List<T> list, int left, int right, int k) {
    	// find the partition
        int partition = partition(list, left, right);
        
        // if partition value is equal to the kth position,
        // return value at k.
        if (partition == k - 1)
            return (int) list.get(partition);

        // if partition value is less than kth position,
        // search right side of the array.
        else if (partition < k - 1)
            return select(list, partition + 1, right, k);
        
        // if partition value is more than kth position,
        // search left side of the array.
        else
            return select(list, left, partition - 1, k);
    }
    
    private <T> void swap(List<T> list, int index0, int index1) {
    	T temp;
    	T left = list.get(index0);
    	T right = list.get(index1);
    	
    	temp = list.set(index1, left);
    	list.set(index0, right);
    	list.set(index1, temp);

	}
    

}
